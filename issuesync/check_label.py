#!/usr/bin/env python3
# Copyright (c) 2019-2021 Collabora, Ltd.
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>
#
# Requires Python 3
"""
Find GitHub issues marked as "synced" but that aren't in our database.

For ones we can't solve on our own, we can output a sync-config.json file
for sync_finish.py to process after human intervention.
"""

import json
from typing import Any, Dict, Mapping, Optional, List

from graphqlclient.client import GraphQLClient

from issuesync.items import (GitHubItem, GitHubItemIdentity,
                             GitLabIssueIdentity, TagRegExParser)

from . import data, github
from .data import ProjectData
from .github import get_graphql
from .sync_start import CONFIG

# TAG_RE = re.compile(
#     r"\[KHR:(?P<ref>(?P<repo>[^#!]+)(?P<typechar>#|!)(?P<num>[0-9]+))\]")
# assert(TAG_RE.match('[KHR:openxr/openxr#1235]'))

_LABELED_ISSUES_QUERY = get_graphql(__name__, "labeled_issues.graphql")
_ISSUE_COMMENTS_QUERY = get_graphql(__name__, "issue_comments.graphql")
_ITEM_AUTHOR_ID_BODY_URL_QUERY = get_graphql(
    __name__, "item_author_id_body_url.graphql")


class ProcessProject:
    def __init__(self, d, gh_client, proj_config):
        self.data = d
        self.gh_client = gh_client
        self.proj_config = proj_config
        self.approved_commenters = set(proj_config['approved_commenters'])
        self.owner, self.repo = proj_config['owner'], proj_config['repo']
        self.label_name = proj_config['label_name']
        self.proj_data: ProjectData = d.get_project(self.owner, self.repo)
        self._tag_parser = TagRegExParser(proj_config)

    def get_unknown_synced_issues(self) -> List[GitHubItem]:
        results = []
        params = {
            'owner': self.owner,
            'repo': self.repo,
            'pagesize': github.ISSUE_LIST_PAGE_SIZE,
            'states': ['OPEN'],
            'labelname': self.label_name}
        for issue in github.nodes_from_paginated(
                self.gh_client, _LABELED_ISSUES_QUERY, params,
                ('data', 'repository', 'label', 'issues')):
            issue_num = issue['number']
            if self.proj_data.is_synced(issue_num):
                continue
            results.append(GitHubItem.from_graphql(
                issue, owner=self.owner, repo=self.repo))

        return results

    def find_issue_sync(self, item: GitHubItem) -> Optional[GitLabIssueIdentity]:
        # First try the body - it might have been created by someone trusted
        # and have a tag.
        item_data = self.get_issue_author_and_body(item.identity.number)
        corresponding_issue = self.find_ref_in_comment(
            item.identity, item_data)
        if corresponding_issue:
            return corresponding_issue

        # OK, then we'll look at comments
        params = {
            'owner': self.owner,
            'repo': self.repo,
            'pagesize': 5,
            'num': item.identity.number}
        for comment in github.nodes_from_paginated(
                self.gh_client, _ISSUE_COMMENTS_QUERY, params,
                ('data', 'repository', 'issue', 'comments')):
            corresponding_issue = self.find_ref_in_comment(
                item.identity, comment)
            if corresponding_issue:
                return corresponding_issue

        print("Couldn't find a sync match for", item.identity,
              "- please add an appropriately formatted comment to",
              item.url)
        return None

    def record_issue_sync_if_possible(self, item: GitHubItem) -> bool:
        matching_issue_id = self.find_issue_sync(item)
        if matching_issue_id:
            self.proj_data.add_sync_entry(
                item.identity, matching_issue_id)
            return True
        return False

    def find_ref_in_comment(self, item: GitHubItemIdentity,
                            comment: Mapping[str, Any]) -> Optional[GitLabIssueIdentity]:
        if comment['author']['login'] not in self.approved_commenters:
            return None
        gl_issue_id = self._tag_parser.parse(comment['body'])
        if gl_issue_id:
            print("GitHub", item, "sync with GitLab issue", gl_issue_id)
        return gl_issue_id

    def get_issue_author_and_body(self, issue_num: int) -> Mapping:
        params = {
            'owner': self.owner,
            'repo': self.repo,
            'number': issue_num}
        parsed = github.execute_graphql(
            self.gh_client, _ITEM_AUTHOR_ID_BODY_URL_QUERY, params)
        return parsed['data']['repository']['issueOrPullRequest']

    def get_issue_url(self, issue_num: int) -> str:
        params = {
            'owner': self.owner,
            'repo': self.repo,
            'number': issue_num}
        parsed = github.execute_graphql(
            self.gh_client, _ITEM_AUTHOR_ID_BODY_URL_QUERY, params)
        return parsed['data']['repository']['issueOrPullRequest']['url']


def check_label(d, config: data.Config,
                gh_client: GraphQLClient,
                sync_config_fn: str):
    output: Dict[str, Any] = {
        '$schema': data.SYNC_CONFIG_SCHEMA,
        'comment': (
            "Manually populate the fields for those you want to comment on.")
    }
    has_missing_sync = False
    for path, proj in config.by_path.items():
        process_project = ProcessProject(d, gh_client, proj)
        items = process_project.get_unknown_synced_issues()

        project_add_comments = []
        for item in items:
            if not process_project.record_issue_sync_if_possible(item):
                partial_data = item.identity.to_dict()
                partial_data['url'] = item.url
                partial_data['gl_issue'] = None
                project_add_comments.append(partial_data)
        if project_add_comments:
            has_missing_sync = True
            output[path] = {
                "add_comments": [project_add_comments]
            }

    d.save()

    if has_missing_sync:
        with open(sync_config_fn, 'w', encoding='utf-8') as fp:
            json.dump(output, fp, indent=4)
        print("Edit {} then run issue_sync finish".format(sync_config_fn))


def main():
    d = data.Data()
    config = data.Config()
    gh_client = github.make_gh_client()
    check_label(d, config, gh_client, CONFIG)


if __name__ == "__main__":
    main()
