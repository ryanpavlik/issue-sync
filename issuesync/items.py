# Copyright (c) 2019-2021 Collabora, Ltd.
# SPDX-License-Identifier: Apache-2.0
import dataclasses
import enum
import json
import re
from dataclasses import dataclass
from os import name
from typing import (Any, Collection, Dict, Iterable, Mapping, Optional, Set,
                    Tuple, Union, cast)


class GitHubItemType(str, enum.Enum):
    ISSUE = "Issue"
    PULL_REQUEST = "Pull Request"


def _try_get_dict_path(d: Mapping[str, Any], path: Iterable[str]) -> Union[str, Mapping[str, Any], None]:
    current = d
    for level in path:
        try:
            if current is None or level not in current:
                return None
            current = current[level]
        except KeyError:
            return None
    return current


@dataclass
class GitLabIssueIdentity:
    project_id: Optional[int]
    number: int
    owner: Optional[str] = None
    repo: Optional[str] = None

    @classmethod
    def from_api_response(cls, data_dict: Mapping[str, Any],
                          owner: Optional[str] = None,
                          repo: Optional[str] = None):
        return cls(
            project_id=data_dict['project_id'],
            number=data_dict['iid'],
            owner=owner,
            repo=repo
        )

    @classmethod
    def from_dict(cls, data_dict: Mapping[str, Any],
                  owner: Optional[str] = None,
                  repo: Optional[str] = None):
        if not owner:
            owner = data_dict.get('owner')
        if not repo:
            repo = data_dict.get('repo')
        return cls(
            project_id=data_dict.get('project_id'),
            number=data_dict['number'],
            owner=owner,
            repo=repo
        )

    def to_dict(self) -> Dict[str, Any]:
        return dataclasses.asdict(self)


@dataclass
class GitLabIssue:
    identity: GitLabIssueIdentity
    url: Optional[str] = None
    title: Optional[str] = None
    body: Optional[str] = None
    labels: Optional[Collection[str]] = None

    @classmethod
    def from_api_response(cls, data_dict: Mapping[str, Any],
                          owner: Optional[str] = None,
                          repo: Optional[str] = None, **kwargs):
        identity = GitLabIssueIdentity.from_api_response(
            data_dict, owner, repo)
        return cls(
            identity=identity,
            url=data_dict.get('web_url'),
            **kwargs
        )

    def to_dict(self) -> Dict[str, Any]:
        # merge with fields from the id
        result = self.identity.to_dict()
        result.update({
            k: v
            for k, v in dataclasses.asdict(self).items()
            if k != 'identity'})
        return result


GitHubItemIdentityTuple = Tuple[str, str, int, Optional[str]]
GitHubItemIdentityShortTuple = Tuple[str, str, int]


@dataclass
class GitHubItemIdentity:
    owner: str
    repo: str
    number: int
    id_str: Optional[str] = None

    @classmethod
    def from_graphql(cls, data_dict: Mapping[str, Any],
                     owner: Optional[str] = None, repo: Optional[str] = None):
        if not owner:
            name_with_owner = _try_get_dict_path(
                data_dict, ('repository', 'nameWithOwner'))
            if name_with_owner:
                owner_and_name = str(name_with_owner).split('/')
                owner = owner_and_name[0]
                if not repo:
                    repo = name
        if not owner:
            maybe_owner = _try_get_dict_path(
                data_dict, ('repository', 'owner', 'login'))
            if maybe_owner:
                owner = str(maybe_owner)
        if not repo:
            maybe_repo = _try_get_dict_path(data_dict, ('repository', 'name'))
            if maybe_repo:
                repo = str(maybe_repo)
        if not owner:
            raise RuntimeError("Could not determine owner name")
        if not repo:
            raise RuntimeError("Could not determine repo name")

        return cls(
            owner=owner,
            repo=repo,
            number=data_dict['number'],
            id_str=data_dict.get('id')
        )

    @classmethod
    def from_json_dict(cls, data_dict: Mapping[str, Any]):
        owner: str = data_dict['owner']
        repo: str = data_dict['repo']
        number: int = data_dict['number']
        id_str: Optional[str] = data_dict.get('id_str')

        return cls(
            owner=owner,
            repo=repo,
            number=number,
            id_str=id_str,
        )

    def to_tuple(self) -> GitHubItemIdentityTuple:
        return cast(GitHubItemIdentityTuple, dataclasses.astuple(self))

    def to_short_tuple(self) -> GitHubItemIdentityShortTuple:
        """Like to_tuple() but omits the id_str, since not all have this."""
        return self.to_tuple()[:-1]

    def to_both_tuples(self) -> Set[Union[GitHubItemIdentityTuple,
                                          GitHubItemIdentityShortTuple]]:
        return {self.to_tuple(), self.to_short_tuple()}

    def to_dict(self) -> Dict[str, Any]:
        return dataclasses.asdict(self)


@dataclass
class GitHubItem:
    identity: GitHubItemIdentity
    item_type: Optional[GitHubItemType] = None
    labels: Optional[Collection[str]] = None
    url: Optional[str] = None
    title: Optional[str] = None
    body: Optional[str] = None
    connected_issues: Optional[Collection[int]] = None

    @property
    def lower_item_type(self):
        if self.item_type is not None:
            return self.item_type.lower()
        return "issue"

    @classmethod
    def from_graphql(cls, data_dict: Mapping[str, Any],
                     owner: Optional[str] = None,
                     repo: Optional[str] = None):
        identity = GitHubItemIdentity.from_graphql(data_dict, owner, repo)

        optional_keys = ('url', 'body', 'title')
        kwargs = {key: data_dict.get(key) for key in optional_keys}

        # Try to guess the type
        typename = data_dict.get('__typename')
        if typename == 'PullRequest':
            kwargs['item_type'] = GitHubItemType.PULL_REQUEST
        elif typename == 'Issue':
            kwargs['item_type'] = GitHubItemType.ISSUE

        # Try to get the labels
        labels_nodes = _try_get_dict_path(data_dict, ('labels', 'nodes'))
        if labels_nodes is not None:
            # TODO this cast isn't great, better to check the input, but...
            labels_nodes = cast(Iterable[Mapping[str, str]], labels_nodes)
            kwargs['labels'] = [x['name']
                                for x in labels_nodes]
        # Try to get connected issues for a pull request
        timeline_items_nodes = _try_get_dict_path(
            data_dict, ('timelineItems', 'nodes'))
        if timeline_items_nodes:
            # TODO this cast isn't great, better to check the input, but...
            timeline_items_nodes = cast(
                Iterable[Mapping[str, Mapping[str, Any]]],
                timeline_items_nodes)
            print(timeline_items_nodes)
            connected = [GitHubItemIdentity.from_graphql(x['subject'])
                         for x in timeline_items_nodes]
            connected.extend(GitHubItemIdentity.from_graphql(x['source'])
                             for x in timeline_items_nodes)

            kwargs['connected_issues'] = [x for x in connected
                                          if x != identity]
        return cls(
            identity=identity,
            **kwargs
        )

    @classmethod
    def from_json_dict(cls, data_dict: Mapping[str, Any]):
        fields = [x.name
                  for x in dataclasses.fields(cls)
                  if x.name != 'identity']
        kwargs = {field: data_dict.get(field) for field in fields}
        return cls(identity=GitHubItemIdentity.from_json_dict(data_dict),
                   **kwargs)

    def to_dict(self) -> Dict[str, Any]:
        # merge with fields from the id
        result = self.identity.to_dict()
        result.update({
            k: v
            for k, v in dataclasses.asdict(self).items()
            if k != 'identity'})
        return result

    def to_json(self):
        return json.dumps(self.to_dict())


class TagRegExParser:
    def __init__(self, proj_config: dict) -> None:
        self._re = re.compile(proj_config['tag_re'])

    def parse(self, body: str) -> Optional[GitLabIssueIdentity]:
        match = self._re.search(body)
        if not match:
            return None
        ref = match.group('ref')
        gl_num = int(match.group('num'))
        if match.group('typechar') != '#':
            raise RuntimeError('match is an MR, not an issue: ' + ref)
        owner, repo = match.group('repo').rsplit('/')
        return GitLabIssueIdentity(project_id=None,
                                   owner=owner,
                                   repo=repo,
                                   number=gl_num)


@dataclass
class SyncEntry:
    original: GitHubItemIdentity
    synced_to: GitLabIssueIdentity
