#!/usr/bin/env python3
# Copyright (c) 2019-2021 Collabora, Ltd.
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>
#
# Requires Python 3.7 or newer
"""Close any GitLab issues whose matching GitHub issue is closed."""

import asyncio

import aiohttp
from gidgetlab.aiohttp import GitLabAPI

from . import data, github
from .github import get_graphql
from .gitlab import GitLabAccess, GitLabProject, make_gl_client
from .log import config_logger


class ProcessProject:
    def __init__(self, d, gh_client, gl_access: GitLabAccess,
                 gl: GitLabAPI, proj_config):
        self.data = d
        self.gh_client = gh_client
        self.proj_config = proj_config
        self.gl_proj = GitLabProject(gl_access, proj_config['gitlab'])
        self.gl = gl
        self.owner, self.repo = proj_config['owner'], proj_config['repo']
        self.label_name = proj_config['label_name']
        self.proj_data = d.get_project(self.owner, self.repo)

    def get_synced_closed_issues(self):
        results = []
        query = get_graphql(__name__, "labeled_issues.graphql")
        params = {
            'owner': self.owner,
            'repo': self.repo,
            'pagesize': github.ISSUE_LIST_PAGE_SIZE,
            'states': ['CLOSED'],
            'labelname': self.label_name,
        }
        for issue in github.nodes_from_paginated(
                self.gh_client, query, params,
                ('data', 'repository', 'label', 'issues')):
            num = issue['number']
            if not self.proj_data.is_synced(num):
                continue
            results.append((num, issue['state']))

        query = get_graphql(__name__, "labeled_pulls.graphql")
        params['states'] = ['CLOSED', 'MERGED']
        for pull in github.nodes_from_paginated(
                self.gh_client, query, params,
                ('data', 'repository', 'label', 'pullRequests')):
            num = pull['number']
            if not self.proj_data.is_synced(num):
                continue
            results.append((num, pull['state']))

        return results

    async def close_corresponding_issue_if_open(self, gh_num, state: str):
        gl_num = self.proj_data.get_gitlab_issue_num(gh_num)
        results = await self.gl_proj.get_gitlab_item(self.gl,
                                                     f'/issues/{gl_num}')
        if results['state'] == 'closed':
            # Skip this one.
            return
        await self.gl_proj.post_gitlab(self.gl, f'/issues/{gl_num}/notes', {
            'body':
            "Closing this issue because the associated "
            f"GitHub item was {state.lower()}."
        })
        await self.gl_proj.put_gitlab(self.gl,
                                      f'/issues/{gl_num}',
                                      {'state_event': 'close'})
        print(
            f"Closed GitLab issue {gl_num} because GitHub issue {gh_num} "
            f"on {self.repo} was {state.lower()}.")


async def async_reverse_sync(d, config, gh_client, gl_access: GitLabAccess):
    async with aiohttp.ClientSession() as session:

        gl = gl_access.make_api(session)
        for proj in config.by_path.values():
            process_project = ProcessProject(d, gh_client, gl_access, gl, proj)
            issue_nums = process_project.get_synced_closed_issues()
            # print(issue_nums)
            tasks = []
            for gh_num, state in issue_nums:
                tasks.append(asyncio.create_task(
                    process_project.close_corresponding_issue_if_open(gh_num,
                                                                      state)))
            await asyncio.gather(*tasks)


def reverse_sync(d, config, gh_client, gl_access: GitLabAccess):
    asyncio.run(async_reverse_sync(d, config, gh_client, gl_access))


def main():
    config_logger()
    d = data.Data()
    config = data.Config()
    gh_client = github.make_gh_client()
    gl_client = make_gl_client()
    reverse_sync(d, config, gh_client, gl_client)


if __name__ == "__main__":
    main()
