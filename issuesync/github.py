#!/usr/bin/env python3 -i
# Copyright (c) 2019 Collabora, Ltd.
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>
#
# Requires Python 3

import json
import os

import pkg_resources
from graphqlclient import GraphQLClient

ISSUE_LIST_PAGE_SIZE = 5


def get_graphql(name, fn):
    return pkg_resources.resource_string(name, 'graphql/' + fn).decode('utf-8')


def make_gh_client(token=None) -> GraphQLClient:
    if not token:
        from dotenv import load_dotenv
        load_dotenv()
        token = os.getenv('AUTH_TOKEN', None)

    client = GraphQLClient('https://api.github.com/graphql')
    client.inject_token("token " + token)
    return client


def dict_path(d, path):
    for level in path:
        d = d[level]
    return d


def execute_graphql(client: GraphQLClient, query: str, params: dict):
    result = client.execute(query, params)
    parsed = json.loads(result)
    if 'error' in parsed:
        raise RuntimeError(
            "Got an error in GraphQL execution: " +
            json.dumps(parsed['error']))
    if 'errors' in parsed:
        raise RuntimeError(
            "Got an error in GraphQL execution: " +
            json.dumps(parsed['errors']))
    return parsed


def paginated(gh_client, query, params, page_info_path):
    """Generates all paginated results of a GraphQL query.

    gh_client: GraphQL client (intended for GitHub but possibly compatible
    elsewhere)
    query: A named query with parameters, at least one of which must be
    '$after: String', with first: someArbitraryNumber, after: $after
    as parameters to the paginated query,
    and with pageInfo { hasNextPage, endCursor } in it.
    params: Any other parameters you want to pass. This dict will be modified.
    page_info_path: A list or tuple of the dictionary keys to get to the
    pageInfo.
    """
    after = None
    while True:
        params['after'] = after
        parsed = execute_graphql(gh_client, query, params)
        yield parsed

        page_info = dict_path(parsed, page_info_path)
        if not page_info['hasNextPage']:
            break
        after = page_info['endCursor']


def nodes_from_paginated(client, query, params, pagination_path):
    """Generates all nodes from a paginated GraphQL query.

    Like paginated but yields the nodes you want directly.
    pagination_path should be the path that contains both 'nodes' and
    'pageInfo'."""
    after = None
    while True:
        params['after'] = after
        parsed = execute_graphql(client, query, params)
        pagination = dict_path(parsed, pagination_path)
        for node in pagination['nodes']:
            yield node
        page_info = pagination['pageInfo']
        if not page_info['hasNextPage']:
            break
        after = page_info['endCursor']


class Project:
    def __init__(self, json_data):
        self.owner = json_data['owner']
        self.repo = json_data['repo']
        self.label_name = json_data['label_name']
        self.approved_commenters = set(json_data['approved_commenters'])

    def common_params(self):
        return {"owner": self.owner, "repo": self.repo}

    def labeled_issues(self, gh_client):
        query = get_graphql(__name__, "labeled_issues.graphql")
        params = {
            "labelname": self.label_name,
            "states": ["OPEN"],
        }
        params.update(self.common_params())
        data = []
        for parsed in paginated(gh_client, query, params,
                                ('data',
                                 'repository',
                                 'label',
                                 'issues',
                                 'pageInfo')):
            issues = parsed['data']['repository']['label']['issues']
            data.extend(issue['number'] for issue in issues['nodes'])

        return data

    def get_open_issues_and_labels(self, gh_client):
        query = get_graphql(__name__, "open_issues.graphql")
        params = self.common_params()
        data = []
        for parsed in paginated(gh_client, query, params,
                                ('data',
                                 'repository',
                                 'issues',
                                 'pageInfo')):
            issues = parsed['data']['repository']['issues']
            for issue in issues['nodes']:
                labels = set(node['name'] for node in issue['labels']['nodes'])
                data.append({
                    "title": issue['title'],
                    'url': issue['url'],
                    'number': issue['number'],
                    'labels': labels
                })

        return data


def _openxr_docs():
    return Project({
        'owner': "KhronosGroup",
        'repo': "OpenXR-Docs",
        'label_name': 'synced to gitlab',
        "approved_commenters": ('rpavlik', 'rpavlik-bot')
    })


def __main():
    client = make_gh_client()
    xrdocs = _openxr_docs()
    print(xrdocs.labeled_issues(client))
    print(xrdocs.get_open_issues_and_labels(client))


if __name__ == "__main__":
    __main()
