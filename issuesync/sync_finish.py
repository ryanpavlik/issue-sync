#!/usr/bin/env python3
# Copyright (c) 2019-2024 Collabora, Ltd.
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>
#
# Requires Python 3.7 or newer
"""Handle the human-modified sync-config.json file created by other scripts."""
import json
from typing import Any, Mapping

from graphqlclient.client import GraphQLClient

from issuesync.items import GitHubItemIdentity

from . import data, github
from .github import execute_graphql, get_graphql
from .gitlab import GitLabAccess, GitLabProject, make_gl_client
from .items import GitHubItem, GitLabIssue, GitLabIssueIdentity, TagRegExParser
from .sync_start import CONFIG

_ITEM_AUTHOR_ID_BODY_URL_QUERY = get_graphql(__name__,
                                             "item_author_id_body_url.graphql")

_LABEL_ID_QUERY = get_graphql(__name__, "label_id.graphql")

_ADD_LABEL_AND_COMMENT_MUTATION = get_graphql(__name__,
                                              "add_label_and_comment.graphql")


def load_sync_config(fn):
    with open(fn, 'r', encoding='utf-8') as fp:
        return json.load(fp)


def save_sync_config(config, fn):
    with open(fn, 'w', encoding='utf-8') as fp:
        json.dump(config, fp, indent=4)


def get_issue_description(client, owner, repo, number):
    params = {'owner': owner, 'repo': repo, 'number': number}
    parsed = execute_graphql(
        client, _ITEM_AUTHOR_ID_BODY_URL_QUERY, params)
    return parsed['data']['repository']['issueOrPullRequest']['body']


def get_label_id(client, owner, repo, name):
    params = {
        'owner': owner,
        'repo': repo,
        'name': name,
    }
    parsed = execute_graphql(client, _LABEL_ID_QUERY, params)
    return parsed['data']['repository']['label']['id']


def get_issue_id(client, owner, repo, num) -> GitHubItemIdentity:
    params = {
        'owner': owner,
        'repo': repo,
        'num': num,
    }
    parsed = execute_graphql(client, _ITEM_AUTHOR_ID_BODY_URL_QUERY, params)
    return GitHubItemIdentity.from_graphql(
        parsed['data']['repository']['issueOrPullRequest'])


class ProcessProject:
    def __init__(self, gh_client, gh_proj_path, proj_config: Mapping[str, Any],
                 proj_data: data.ProjectData,
                 gl_proj: GitLabProject, dry_run=False):
        self.gh_client = gh_client
        self.gh_proj_path = gh_proj_path
        self.proj_config = proj_config
        self.comment_template = '\n'.join(proj_config['comment_template'])
        self.proj_data = proj_data
        self.gl_proj = gl_proj
        self.dry_run = dry_run

        self.owner = proj_config['owner']
        self.repo = proj_config['repo']

        self.gh_label_id = get_label_id(
            gh_client, self.owner, self.repo, self.proj_config['label_name'])
        self.gl_label_name = self.proj_config['gitlab']['label_name']
        if isinstance(self.gl_label_name, str):
            self.gl_label_name = [self.gl_label_name]
        self.gl_proj_path = self.proj_config['gitlab']['full_path']

    def add_sync_comment_to_issue(self, gh_item: GitHubItem,
                                  gl_issue: GitLabIssue):
        text = self.comment_template.format(
            gl_issue_num=gl_issue.identity.number,
            gl_proj_path=self.gl_proj_path,
            gl_issue_url=gl_issue.url,
            gh_item_type_lower=gh_item.lower_item_type)
        assert (isinstance(text, str))
        params = {
            'id': gh_item.identity.id_str,
            'label_id': self.gh_label_id,
            'text': text,
        }
        if self.dry_run:
            print("Would execute graphql:",
                  _ADD_LABEL_AND_COMMENT_MUTATION, params)
        else:
            print("Adding comment to", gh_item.identity)
            _ = execute_graphql(
                self.gh_client, _ADD_LABEL_AND_COMMENT_MUTATION, params)

    def sync_issue(self, gh_issue: GitHubItem):
        if self.proj_data.is_synced(gh_issue.identity):
            print("Issue already synced!", gh_issue.identity)
            return
        if gh_issue.body is None:
            desc = get_issue_description(
                self.gh_client,
                self.proj_config['owner'],
                self.proj_config['repo'],
                number=gh_issue.identity.number)
            gh_issue.body = desc
        gl_new_issue = self.file_internal_issue(gh_issue)
        if not gl_new_issue:
            return

        # print(gl_new_issue)
        self.add_sync_comment_to_issue(
            gh_issue, gl_new_issue)
        if not self.dry_run:
            self.proj_data.add_sync_entry(
                gh_issue.identity, gl_new_issue.identity)

    def file_internal_issue(self, gh_item: GitHubItem):
        title = "[{} {} {}] {}".format(
            self.gh_proj_path, gh_item.lower_item_type,
            gh_item.identity.number, gh_item.title)
        description_lines = ["GitHub {}: <{}>".format(
            str(gh_item.item_type), gh_item.url)]

        if gh_item.body:
            description_lines.append('\nDescription at sync time:\n')
            quoted = ('> ' + line
                      for line
                      in gh_item.body.replace('\r', '').split('\n'))
            description_lines.extend(quoted)
        description = "\n".join(description_lines)
        if self.dry_run:
            new_issue = GitLabIssue(
                GitLabIssueIdentity(project_id=-1, number=-1),
                title=title,
                body=description
            )
            print("Would file new issue:")
            print("title:", title)
            print("description:", description)
        else:
            new_issue_data = self.gl_proj.post_gitlab_sync(
                '/issues',
                {'title': title,
                 'description': description,
                 'labels': self.gl_label_name}
            )
            new_issue = GitLabIssue.from_api_response(new_issue_data)
            new_issue.body = description
            new_issue.title = title
        new_issue.labels = self.gl_label_name
        return new_issue


def finish(d: data.Data, config: data.Config, gl_access: GitLabAccess,
           gh_client: GraphQLClient, sync_file_name: str,
           dry_run: bool = False):
    sync_config = load_sync_config(sync_file_name)
    for proj_path, proj_sync_config in sync_config.items():
        # Per-project loop
        if proj_path not in config.by_path:
            # It's a comment.
            print(f"Skipping '{proj_path}' - not a GitHub project path")
            continue
        proj_config = config.by_path[proj_path]
        process_project = ProcessProject(gh_client,
                                         proj_path,
                                         proj_config,
                                         d.get_project(proj_path),
                                         GitLabProject(
                                             gl_access, proj_config['gitlab']),
                                         dry_run=dry_run)

        reference_parser = TagRegExParser(proj_config)
        # from sync_start
        for entry in proj_sync_config.get('ignore', []):
            entry = GitHubItem.from_json_dict(entry)
            process_project.proj_data.ignore(entry.identity)
        if 'ignore' in proj_sync_config:
            del proj_sync_config['ignore']

        # from sync_start
        for entry in proj_sync_config.get('sync', []):
            entry = GitHubItem.from_json_dict(entry)
            process_project.sync_issue(entry)
        if 'sync' in proj_sync_config:
            del proj_sync_config['sync']

        # from check_label
        for entry in proj_sync_config.get('add_comments', []):
            gl_issue_ref = entry['gl_issue']
            gl_issue_id = None
            if isinstance(gl_issue_ref, str):
                gl_issue_id = reference_parser.parse(gl_issue_ref)
            elif isinstance(gl_issue_ref, int):
                gl_issue_id = GitLabIssueIdentity(
                    project_id=None, owner=proj_config['owner'],
                    repo=proj_config['repo'], number=gl_issue_ref)
            gh_item = GitHubItem.from_json_dict(entry)
            if not gl_issue_id:
                continue
            if process_project.proj_data.is_synced(gh_item.identity.number):
                print("Issue already synced!", gh_item.identity)
                continue
            process_project.add_sync_comment_to_issue(
                gh_item, gl_issue_ref)
            process_project.proj_data.add_sync_entry(
                gh_item.identity, gl_issue_id)
        if 'add_comments' in proj_sync_config:
            del proj_sync_config['add_comments']

    d.save()
    save_sync_config(sync_config, sync_file_name)


def main():
    d = data.Data()
    config = data.Config()
    gh_client = github.make_gh_client()
    gl_client = make_gl_client()
    finish(d, config, gl_client, gh_client, CONFIG)


if __name__ == "__main__":
    main()
