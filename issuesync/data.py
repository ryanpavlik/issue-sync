#!/usr/bin/env python3 -i
# Copyright (c) 2019-2021 Collabora, Ltd.
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>
#
# Requires Python 3

import json
from typing import Any, Dict, List, Mapping, Optional, Set, Tuple, Union

from .items import GitHubItemIdentity, GitLabIssueIdentity

_IGNORED = 'ignored'
_SYNCED = 'synced'
_ORIGINAL = 'original'
_SYNCED_ITEMS = 'synced_items'

SYNC_CONFIG_SCHEMA = "./sync-config.schema.json"


def _make_default_initial_contents():
    return {
        "$schema": "./data.schema.json",
        "comment": "This is the script-maintained data storage.",
    }


class ProjectData:
    def __init__(self, proj: Mapping[str, Any], owner: str, repo: str):
        self.proj = proj
        self.owner, self.repo = owner, repo

        def make_item_id_dict(x: dict):
            result = {'owner': owner, 'repo': repo}
            result.update(x)
            return result
        ignored_identities: List[GitHubItemIdentity] = [
            GitHubItemIdentity.from_json_dict(
                make_item_id_dict(x)
            )
            for x in self.proj[_IGNORED]
            if not isinstance(x, int)
        ]
        ignored_identities.extend(
            GitHubItemIdentity(owner=owner, repo=repo, number=x)
            for x in self.proj[_IGNORED]
            if isinstance(x, int))
        # print(ignored_identities)
        self._ignored: Set[Union[Tuple[str, str, int], Tuple[str, str, int, Optional[str]]]] = {
            x.to_tuple()
            for x in ignored_identities
        }
        self._ignored.update({
            x.to_short_tuple()
            for x in ignored_identities
        })
        # print(self._ignored)

    def is_ignored(self, item: GitHubItemIdentity):
        return not self._ignored.isdisjoint(item.to_both_tuples())

    def ignore(self, item: GitHubItemIdentity):
        tupled = item.to_tuple()
        if not self.is_ignored(item):
            self.proj[_IGNORED].append(item.to_dict())
            self._ignored.add(tupled)

    def add_sync_entry(self, gh_item_id: GitHubItemIdentity,
                       gl_issue_id: GitLabIssueIdentity):
        self.proj[_SYNCED][str(gh_item_id.number)] = gl_issue_id.number
        self.proj[_SYNCED_ITEMS].append({
            _ORIGINAL: gh_item_id.to_dict(),
            _SYNCED: gl_issue_id.to_dict(),
        })

    def is_synced(self, gh_num):
        return str(gh_num) in self.proj[_SYNCED]

    def get_gitlab_issue_num(self, gh_num):
        return self.proj[_SYNCED].get(str(gh_num))


class Data:
    def __init__(self, fn: Optional[str] = None):
        if fn:
            self.fn = fn
        else:
            # self.fn = Path(__file__).resolve().parent / "data.json"
            self.fn = 'data.json'

        self.data = _make_default_initial_contents()
        try:
            with open(str(self.fn), 'r', encoding='utf-8') as fp:
                self.data.update(json.load(fp))
        except FileNotFoundError:
            # already have defaults
            pass

    def save(self):
        with open(str(self.fn), 'w', encoding='utf-8') as fp:
            json.dump(self.data, fp, indent=4)

    def get_project(self, path_or_owner: str,
                    repo: Optional[str] = None) -> ProjectData:
        "Get an object with per-project settings"
        if repo is None:
            proj = path_or_owner
            owner, repo = proj.split('/')
        else:
            proj = '{}/{}'.format(path_or_owner, repo)
            owner = path_or_owner
        if proj not in self.data:
            self.data[proj] = {}

        d = self.data[proj]

        def make_default(name, val):
            if name not in d:
                d[name] = val

        make_default(_IGNORED, [])
        make_default(_SYNCED, {})
        make_default(_SYNCED_ITEMS, [])

        return ProjectData(d, owner, repo)


class Config:
    def __init__(self, fn: Optional[str] = None):
        if fn:
            self.fn = fn
        else:
            # self.fn = Path(__file__).resolve().parent / "data.json"
            self.fn = 'config.json'
        with open(str(self.fn), 'r', encoding='utf-8') as fp:
            self.data = json.load(fp)

        tag_re: Optional[str] = self.data.get("tag_re")
        comment_template: Optional[str] = self.data.get(
            "comment_template")
        self.by_owner_and_repo: Dict[Tuple[str, str], Dict] = {
        }
        self.by_path: Dict[str, Dict] = {}
        for proj in self.data['repos']:
            # Inherit from global if not otherwise specified.
            proj.setdefault('tag_re', tag_re)
            proj.setdefault('comment_template', comment_template)

            owner, repo = proj['owner'], proj['repo']
            self.by_owner_and_repo[(owner, repo)] = proj
            self.by_path['{}/{}'.format(owner, repo)] = proj


if __name__ == "__main__":
    d = Data()
    p = d.get_project("KhronosGroup", "OpenXR-Docs")
    p = d.get_project("KhronosGroup/OpenXR-Docs")
    d.save()
