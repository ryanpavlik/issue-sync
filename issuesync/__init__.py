#!/usr/bin/env python3 -i
# Copyright 2020-2024, Collabora, Ltd. and the Proclamation contributors
#
# SPDX-License-Identifier: Apache-2.0
#
# Original author: Rylie Pavlik <rylie.pavlik@collabora.com>
"""Synchronize issues from GitHub to GitLab."""

__version__ = "3.1.0"
