#!/usr/bin/env python3
# Copyright (c) 2019-2021 Collabora, Ltd.
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>
#
# Requires Python 3
"""
List GitHub issues not yet synced to GitLab or ignored.

Find GitHub issues that are not synced to GitLab,
and let a human choose whether to sync, permanently ignore, or temporarily
ignore.

We output a sync-config.json file for sync_finish.py to process after human
intervention.
"""
import json

from . import data, github
from .github import get_graphql
from .items import GitHubItem

CONFIG = 'sync-config.json'


def _generate_items(d, gh_client, proj):
    owner, repo = proj['owner'], proj['repo']
    proj_data = d.get_project(owner, repo)
    query = get_graphql(__name__, "open_issues.graphql")
    params = {'owner': owner, 'repo': repo}
    for issue in github.nodes_from_paginated(gh_client, query, params,
                                             ('data',
                                              'repository',
                                              'issues')):
        item = GitHubItem.from_graphql(issue, owner, repo)
        if proj_data.is_ignored(item.identity):
            continue
        if proj_data.is_synced(item.identity.number):
            continue
        yield item

    query = get_graphql(__name__, "open_pulls.graphql")

    for pull in github.nodes_from_paginated(gh_client, query, params,
                                            ('data',
                                             'repository',
                                             'pullRequests')):
        item = GitHubItem.from_graphql(pull, owner, repo)
        # print(item.connected_issues)

        if item.labels and "dependencies" in item.labels:
            # skip dependabot
            continue
        if proj_data.is_ignored(item.identity):
            continue
        if proj_data.is_synced(item.identity.number):
            continue

        if item.connected_issues:
            # We will assume that if there is any connected issue event,
            # there's already something synced.
            continue
        yield item


def items_needing_sync(d, gh_client, proj):
    results = []
    label_name = proj['label_name']
    for item in _generate_items(d, gh_client, proj):
        # print(item.title, item.labels)
        if label_name not in item.labels:
            results.append(item.to_dict())

    return results


_STARTING_COMMENT = ("Move any issues/pulls you don't want to "
                     "sync to the ignore lists.")


def start(d, config, gh_client, sync_file_name):
    output = {}
    for path, proj in config.by_path.items():
        need_sync = items_needing_sync(d, gh_client, proj)
        if need_sync:
            output[path] = {
                'comment': _STARTING_COMMENT,
                'sync': need_sync,
                'ignore': []
            }

    d.save()

    if output:
        output['$schema'] = data.SYNC_CONFIG_SCHEMA
        with open(sync_file_name, 'w', encoding='utf-8') as fp:
            json.dump(output, fp, indent=4)
        print("Edit {} then run issue_sync finish".format(sync_file_name))
    else:
        print("No issues found to synchronize.")


def main():
    d = data.Data()
    config = data.Config()
    client = github.make_gh_client()
    start(d, config, client, CONFIG)


if __name__ == "__main__":
    main()
