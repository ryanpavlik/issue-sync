# issue-sync

Initially written as a tool for maintaining OpenXR, but you might find it useful
in some other projects too.

## Getting started

```sh
python3 -m venv venv                      # Make a new virtualenv for Python
. venv/bin/activate                       # for bash - other shells vary
pip3 install -e .                         # install ourselves editable
cp sample.env .env                        # then edit .env to put in your own secrets
cp sample-config.json config.json         # and edit to put in your own projects
```

## About the files

- Human authored
  - `config.json` - human authored, specifies projects
  - `.env` - optionally contains secrets. Might be in the environment some other
    way instead.
- Human modified
  - `sync-config.json` - Created by `issue_sync start` and
    `issue_sync check-label`, you should edit it by moving things around or
    removing them (as desired) before running `issue_sync finish`. Can delete
    after running `issue_sync finish`.
- Machine created/modified - humans touch at own risk
  - `data.json` - The "database" of issue states and actions used. Not the end
    of the world if you delete it, but it will need to be re-populated by a
    script and your "ignored" issues will naturally be forgotten again. Try to
    persist this one.
