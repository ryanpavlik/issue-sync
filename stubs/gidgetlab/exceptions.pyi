import http
from typing import Any

class GitLabException(Exception): ...
class ValidationFailure(GitLabException): ...

class HTTPException(GitLabException):
    status_code: Any = ...
    def __init__(self, status_code: http.HTTPStatus, *args: Any) -> None: ...

class RedirectionException(HTTPException): ...
class BadRequest(HTTPException): ...

class RateLimitExceeded(BadRequest):
    rate_limit: Any = ...
    def __init__(self, rate_limit: Any, *args: Any) -> None: ...

class InvalidField(BadRequest):
    errors: Any = ...
    def __init__(self, errors: Any, *args: Any) -> None: ...

class GitLabBroken(HTTPException): ...
